/**
 * Gulp Release Version 7.1.2
 * @author: Vasilis Neris
 */

//
// Modules
//============================
var gulp         = require('gulp'),
    autoprefixer = require('autoprefixer'),
    postcss      = require('gulp-postcss'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    plumber      = require('gulp-plumber'),
    notify       = require('gulp-notify'),
    browserSync  = require('browser-sync').create(),
    exec         = require('child_process').exec;



/**
 * Gulp configuration. Adjust to your needs.
 * Currently not working on live dev enviroment.
 *
 * Configuration Parameters:
 *      src             = Where the watcher will watch for scss changes
 *      dest            = Where the scss files will output to css
 *      base            = Its needed to the structure is not messed up
 *      host            = Your local project url. Adjust it to your needs!
 *      cssFiles        = Where are the css files located so they can get injected in the browser if changed from scss.
 *      proxy           = What you will to the browser. It listens to port 3000
 *      liveReload      = @type boolean. If true it will inject all the css that have been affected by the scss compilation
 *      netsteps_Mode   = **experimental** "default": deletes all pub/static/*. "lite": deletes all .css files in pub static but not styles-l or styles-m.
 */

var config = {
    src           : 'default/web/**/*.scss',
    dest          : 'default/web/css/',
    base          : 'default/web/scss',
    cssFiles      : 'default/web/**/*.css',
    host          : 'calvin.p74.localhost',
    proxy         : 'http://calvin.p74.localhost',
    liveReload    : false,
    netsteps_Mode : 'lite'
};


//
// Documantation/Useful Links
//============================
/**
 gulp-sass: {
        https://devdocs.magento.com/guides/v2.3/frontend-dev-guide/css-topics/gulp-sass.html
        https://www.npmjs.com/package/gulp-sass
        https://web-design-weekly.com/2014/06/15/different-sass-output-styles/
    }
 gulp-sourcemaps: {
        https://www.npmjs.com/package/gulp-sourcemaps
        https://github.com/gulp-sourcemaps/gulp-sourcemaps/wiki/Plugins-with-gulp-sourcemaps-support
        https://devdocs.magento.com/guides/v2.3/frontend-dev-guide/css-topics/custom_preprocess.html
    }
 gulp-watch: {
        https://stackoverflow.com/questions/44412487/delay-in-gulp-watch
        https://stackoverflow.com/questions/39665773/gulp-error-watch-task-has-to-be-a-function
    }
 gulp-browser-sync: {
        https://www.browsersync.io/docs/options
    }
 gulp: {
        https://github.com/gulpjs/gulp/blob/master/docs/recipes/running-tasks-in-series.md
        https://stackoverflow.com/questions/29511491/running-a-shell-command-from-gulp
    }

 */

// Error message
var onError = function (err) {
    notify.onError({
        title   : 'Gulp',
        subtitle: 'Failure!',
        message : 'Error: <%= error.message %>',
        sound   : 'Beep'
    })(err);

    this.emit('end');
};


/**
 * Sass supports four different output styles.
 https://web-design-weekly.com/2014/06/15/different-sass-output-styles/
 :nested
 :compact
 :expanded
 :compressed
 */


// Compile SCSS
gulp.task('styles', function () {
    return  gulp.src(config.src, {base: config.base})
        .pipe(plumber({errorHandler: onError}))
        .pipe(sourcemaps.init({largeFile: true})) //generate Sourcemaps
        .pipe(sass({                      // Compile Scss
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(postcss([ autoprefixer({overrideBrowserslist: ['last 25 versions'], cascade:false}) ])) // Autoprefix your css. Config is in package.json under browserlist
        .pipe(sourcemaps.write('.',{
            addComment: true,
            includeContent:true
        }))
        .pipe(gulp.dest(config.dest)); // output
});


// Gulp Default task. This is what we use. Init from console. ./node_modules/.bin/gulp
gulp.task('default', function() {

    if(config.liveReload){
        browserSync.init({
            proxy: config.proxy,
            host: config.host,
            open: 'external',
            ui: false,
            reloadOnRestart: false,
            ghostMode: false,
            files: [config.cssFiles]
        });
    }

    console.log("\x1b[32m", `\nWatcher Initialized..`);
    console.log("\x1b[32m", `\nPrinting Current Configuration..`);
    console.log(config);

    gulp.watch(config.src).on('change', function(file){
        console.error("\x1b[32m", `\nFile Changed.. ${file} \n`);
    });

    gulp.watch(config.src, { delay:0 }

        ,function(done){

            gulp.series('styles')();

            if(config.netsteps_Mode == 'lite'){
                exec('cd ../../../../;' +
                    'echo "deleting required styles from pub/static/.. \n";' +
                    'pwd;' +
                    'find pub/static/frontend/ -maxdepth 5 -name "*.css" ! -name "styles*.css"  -path "*/css/*" -exec rm {} +;', function (err, stdout, stderr) {
                    console.log(stdout);
                    console.log(stderr);
                    done(err);
                });
            }else{
                exec('cd ../../../../;' +
                    'echo "deleting pub/static/*";' +
                    'rm -rf pub/static/*;', function (err, stdout, stderr) {
                    console.log(stdout);
                    console.log(stderr);
                    done(err);
                });
            }
            done();
        }
    )



});

