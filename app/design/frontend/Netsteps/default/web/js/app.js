define([
    "jquery"
], function ($) {


    require(['jquery'], function ($) {
        $(document).ready(function (e) {


            $('.search-promt-wrapper').on('click', function (ev) {
                $('.block.block-search').addClass('active');
                $('body').addClass('no-overflow');
                $('#search').focus();
            });

            $('.block.block-search > .block.block-content').on('click' , function (ev) {
                if(this == ev.target){
                    $('.block.block-search').removeClass('active');
                    $('body').removeClass('no-overflow');
                }
            });
        });
    })


});