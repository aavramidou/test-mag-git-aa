<?php
/**
 * Copyright © alex All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Netsteps\Lessons\Controller\Netsteps;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResponseInterface;

class MyController extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {

        echo "test";
        die;
    }
}
